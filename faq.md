# Frequently Asked Questions {.unnumbered}

1.  <http://144.214.170.197:8080/> can **NOT** be reached.

    Verify that you are connect from inside the City University of Hong
    Kong.

2.  How do I connect from inside the City University of Hong Kong if
    I\'m outside the university?

    Use the Virtual Private Network (VPN) service provided by the
    university. Setup instructions are provided by the Information
    Technology Services at [Services & Facilities / Virtual Private
    Network
    (VPN)](https://www.cityu.edu.hk/its/services-facilities/virtual-private-network-vpn).

3.  I can **NOT** see the list of projects at
    <http://144.214.170.197:8080/>.

    Are you accessing <http://144.214.170.197:8080/> using
    <https://mvpn.cityu.edu.hk> built-in web browser? If yes, please
    switch to use the Virtual Private Network (VPN) service provided by
    the university. Setup instructions are provided by the Information
    Technology Services at [Services & Facilities / Virtual Private
    Network
    (VPN)](https://www.cityu.edu.hk/its/services-facilities/virtual-private-network-vpn).

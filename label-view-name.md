# View Labeling

The goal here is to extract the view name burned in the radiograph.

## Data to be Labeled

3057 radiographs.

## Data Annotation Specialist

No specialist is required. The annotation was done by:

1. Raniere Gaia Costa da Silva

## Labeling Setup

Use [`triage.xml`](labeling-setup/views.xml).

## Labeling Steps

1.  Open the `HKJC Burn-in Text Radiographs` project.

    ![](screenshot/label-roi-list.png)

2.  Click in the `Label All Tasks`.

    ![](screenshot/label-roi.png)

3.  Left click in the correct view name for the radiograph.

4.  Left click in the `Submit` button in the top right
    corner of the screen. A new radiograph will be loaded and you can
    repeat the process.

## Labeled Data

See [`raw-text-from-with-burned-in-text.csv`](labeling-data/raw-text-from-with-burned-in-text.csv).

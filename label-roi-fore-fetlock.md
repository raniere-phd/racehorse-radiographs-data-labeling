# Fore Fetlock ROI Labeling

The goal here is to create bounding box to describe the spatial location
of the region of interest to a veterinarian.

## Data to be Labeled

24 radiographs representing each of of the views.

## Data Annotation Specialist

Only one specialist is required. The annotation was done by:

1.  Ambika Prasad Mishra

## Labeling Setup

Use [`triage.xml`](labeling-setup/roi.xml).

## Labeling Steps

1.  Open the `ROI` project.

    ![](screenshot/label-roi-list.png)

2.  Click in the `Label All Tasks`.

    ![](screenshot/label-roi.png)

3.  Click in the label `ROI`.

4.  Left click in the radiograph to create one of the corners of the
    bounding box.

5.  Move the mouse to preview the bouding box.

6.  Left click in the radiograph to create the oposite corner of the
    bounding box.

    ![](screenshot/label-roi-with-bounding-box.png)

7.  Left click in the label on the right side of the screen to select
    it.

    ![](screenshot/label-roi-with-options.png)

8.  If need, adjust the bounding box.

9.  If need, rotate the bounding box.

10. If need, add meta information by left clicking in the `+` button in
    the right side of the screen.

11. Left click in the `Submit` button in the top right
    corner of the screen. A new radiograph will be loaded and you can
    repeat the process.

## Labeled Data

The data are available from the Hong Kong Jockey Club but restrictions apply to the availability of these data, which were used under license for the current study, and so are not publicly available. Data are however available from the authors upon reasonable request and with permission of Hong Kong Jockey Club.
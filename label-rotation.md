# Rotation Labeling

The goal here is to have all radiographs in the same orientation.

## Data to be Labeled

198 radiography sets with 48 radiographs each. 9504 radiographs in total.

## Data Annotation Specialist

No specialist is required. The annotation was done by:

1. Raniere Gaia Costa da Silva

## Labeling Setup

Use [`rotation.xml`](labeling-setup/rotation.xml).

## Labeling Steps

1.  Open the `Rotation` project.

    ![](screenshot/label-roi-list.png)

2.  Click in the `Label All Tasks`.

    ![](screenshot/label-roi.png)

3.  Left click in the labels present in the radiograph.

4.  Left click in the `Submit` button in the top right
    corner of the screen. A new radiograph will be loaded and you can
    repeat the process.

## Labeled Data

See [`rotation.csv`](labeling-data/rotation.csv).

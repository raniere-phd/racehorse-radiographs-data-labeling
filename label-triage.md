# Triage Labeling

The goal here is to create statistics of objects and markers present in
each radiograph.

## Data to be Labeled

198 radiography sets with 48 radiographs each. 9504 radiographs in total.

## Data Annotation Specialist

No specialist is required. The annotation was done by:

1. Raniere Gaia Costa da Silva

## Labeling Setup

Use [`triage.xml`](labeling-setup/triage.xml).

## Labeling Steps

1.  Open the `Triage` project.

    ![](screenshot/label-roi-list.png)

2.  Click in the `Label All Tasks`.

    ![](screenshot/label-roi.png)

3.  Left click in the labels present in the radiograph.

4.  Left click in the `Submit` button in the top right
    corner of the screen. A new radiograph will be loaded and you can
    repeat the process.

## Labeled Data

See [`raw-text-from-with-burned-in-text.csv`](labeling-data/raw-text-from-with-burned-in-text.csv).
# Abnormality Labeling

The goal here is to create keypoints to describe the spatial location of
abnormalities identified by a veterinarian.

## Data

142 radiographic sets (6816 radiographs in total).

## Data Annotation Specialist

Three specialist is required for us to conduct a method of agreement.

1.  Christopher M. Riggs
2.  TBD
3.  TBD

## Labeling Setup

Use
[`abnormality.xml`](labeling-setup/abnormality.xml).

## Labeling Steps

1.  Open the `Abnormality` project.

    ![](screenshot/label-abnormality-list.png)

2.  Click in the `Label All Tasks`.

    ![](screenshot/label-abnormality.png)

3.  Click in the label `Abnormality`.

4.  Left click in the radiograph to mark the location of one
    abnormality.

    ![](screenshot/label-abnormality-with-keypoint.png)

5.  Left click in the label on the right side of the screen to select
    it.

    ![](screenshot/label-abnormality-with-options.png)

6.  Add meta information by left clicking in the `+` button in the right
    side of the screen.

7.  Add other abnormalities present in the radiograph and the respect
    meta information.

8.  Left click in the `Submit` button in the top right
    corner of the screen. A new radiograph will be loaded and you can
    repeat the process.

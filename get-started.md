# Get Started

Use your web browser to open <http://144.214.170.197:8080/> (this
address is only accessible from inside the City University of Hong
Kong).

![](screenshot/user-login.png)

You can create your account!

![](screenshot/projects.png)

## Create Project

Add the name of your project, e.g. `Racehorse Radiographs`.

![](screenshot/project-name.png)

Upload the files that need to be label.

![](screenshot/data-import.png)

Choose a template for the labels to be used.

![](screenshot/labeling-setup.png)

Configure the labels to be used. `labeling-setup` has some
code that can be use.

![](screenshot/labeling-setup-choices.png)

Save your project configuration.

## Labeling

All the images are listed in the dashboard.

![](screenshot/dashboard.png)

And the labeling can be perform for each image.

![](screenshot/labeling.png)

## Export Labels

Labels can be exported into different formats, for example JSON and CSV.

![](screenshot/export.png)

That can be imported into another software.

![](screenshot/calc.png)

## Privacy

All data is stored locally in the folder `data`.

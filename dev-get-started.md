# Get Started for Developers

## Prerequisites

-   [Docker](https://docs.docker.com/engine/)
-   [Docker Compose](https://docs.docker.com/compose/)

## Launch Web Server

Run

    $ docker-compose up

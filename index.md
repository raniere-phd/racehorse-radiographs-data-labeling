# Welcome {.unnumbered}

The goal of this project is to label racehorse radiographs using [Label
Studio](https://labelstud.io/) and make the labels available for the researc community. We will use Label Studio because it is
open source and we can run it locally.

## Data Availability

The data are available from the Hong Kong Jockey Club but restrictions apply to the availability of these data, which were used under license for the current study, and so are not publicly available. Data are however available from the authors upon reasonable request and with permission of Hong Kong Jockey Club.
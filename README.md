# Racehorse Radiographs Data Labeling

The goal of this project is
to label racehorse radiographs
using [Label Studio](https://labelstud.io/).
We will use Label Studio
because
it is open source
and
we can run it locally.

More details available at https://raniere-phd.gitlab.io/racehorse-radiographs-data-labeling/.
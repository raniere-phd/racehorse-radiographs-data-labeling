# Labeling

The goal here is to create keypoints to describe the spatial location of
abnormalities in the fore fetlock identified by a veterinarian.

## Data to be Labeled

198 radiograpic sets with

- Fore Fetlock DPa
- Fore Fetlock LM
- Fore Fetlock D45ºL -- PAMO
- Fore Fetlock D45ºM -- PALO
- Fore Fetlock Flexed D125º Distal -- PAPR Oblique
- Fore Fetlock Flexed LM.

1188 radiographs in total.

## Data Annotation Specialist

Three specialist is required for us to conduct a method of agreement.

1.  Christopher M. Riggs
2.  TBD
3.  TBD

## Labeling Setup

Use [`abnormality.xml`](labeling-setup/abnormality.xml).

## Labeling Steps

1.  Open the `HKJC Fore Fetlock Abnormality` project.

    ![](screenshot/label-abnormality-fore-fetlock-projects.png)

2.  Click in the `Label All Tasks`.

    ![](screenshot/label-abnormality-fore-fetlock-list.png)

3.  Click in the label `Abnormality` on the top left. Or press the `1`
    key from the keyboard.

    ![](screenshot/label-abnormality-fore-fetlock.png)

    After you click on it, the label will change it\'s colour.

4.  Left click in the radiograph to mark the location of one
    abnormality.

    ![](screenshot/label-abnormality-fore-fetlock-with-keypoint.png)

    After you click, a red circle will show up where you click.

5.  Left click in the label on the right side of the screen to select
    it.

    ![](screenshot/label-abnormality-fore-fetlock-with-options.png)

6.  Left clicking in the `+` button in the right side of the screen to
    add a description for the abnormality (meta information).

    ![](screenshot/label-abnormality-fore-fetlock-add-meta.png)

    After you click, a text box will show up.

7.  Type a description for the abnormality in the text box.

    ![](screenshot/label-abnormality-fore-fetlock-type-meta.png)

8.  Left click in the `Add` button to save the description for the
    abnormality.

    ![](screenshot/label-abnormality-fore-fetlock-save-meta.png)

    The description for the abnormality will be save and displayed as
    the value of `Meta`.

    To update the description for the abnormality, repeat steps 6\--8.

9.  Left click in the \"compress\" button to unselect the annotation.

    ![](screenshot/label-abnormality-fore-fetlock-close-meta.png)

    The information of the annotation will be hide.

    Or left click in the radiograph to mark the location of another
    abnormality, like in step 4, and go to step 5.

    ![](screenshot/label-abnormality-fore-fetlock-with-keypoint-again.png)

10. After **all** abnormalities in **all** radiographs of the set were
    annotated, left click in the `Submit` button in the top right corner
    of the screen.

    ![](screenshot/label-abnormality-fore-fetlock-submit.png)

    A new radiograph will be loaded and you can repeat the process from
    step 3.

11. If you click on `Submit` by mistake, you can left click on the `<`
    (left arrow) in the top left corner to go back to the previous
    radiographic set.

    ![](screenshot/label-abnormality-fore-fetlock-back.png)

    After you add the missing abnormalities, you can left click on the
    `Update` button on the top right corner and left click on the `>`
    (right arrow) in the top left corner to go back to the newest
    radiographic set.

12. When you annotate **all** the radiographic sets, you will see the
    message `No more annotations`.

    ![](screenshot/label-abnormality-fore-fetlock-end.png)

## Labeled Data

The data are available from the Hong Kong Jockey Club but restrictions apply to the availability of these data, which were used under license for the current study, and so are not publicly available. Data are however available from the authors upon reasonable request and with permission of Hong Kong Jockey Club.